#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

#define WEIGHT_FILE "weights.csv"
#define TRAIN_FILE "train.csv"

class PropertyVector;

vector<PropertyVector> Weights;
vector<PropertyVector> trainData;
int correctClassifications = 0;
vector<double> maxProperties;
vector<double> minProperties;

class PropertyVector{
public:
    PropertyVector(const string& csvLine);
    friend double operator*(const PropertyVector& firstVector , const PropertyVector& secondVector);
    friend void minimum(vector<double>& minProperties , const PropertyVector& vec);
    friend void maximum(vector<double>& maxProperties , const PropertyVector& vec);
    double getBias(void);
    int getPriceRange(void);
    int getSize();
    void normalize(const vector<double>& maxProperties , const vector<double>& minProperties);
    void print();
private:
    vector<double> properties;
    string lastElement;
    vector<string> extractCsvLine(const string& csvLine);
};

int PropertyVector::getSize(){
    return properties.size();
}

void PropertyVector::normalize(const vector<double>& maxProperties , const vector<double>& minProperties){
    for(unsigned int i = 0 ; i < this->properties.size() ; i++)
        this->properties[i] = (this->properties[i] - minProperties[i]) / (maxProperties[i] - minProperties[i]);
}

double operator*(const PropertyVector& firstVector , const PropertyVector& secondVector){
    double result = 0;
    unsigned int size = min(firstVector.properties.size() , secondVector.properties.size() );

    for(unsigned int i = 0 ; i < size ; i++)
        result += (firstVector.properties[i] * secondVector.properties[i]);

    return result;
}

void minimum(vector<double>& minProperties , const PropertyVector& vec){
    for(unsigned int i = 0 ; i < minProperties.size() ; i++)
        minProperties[i] = min(minProperties[i] , vec.properties[i]);
}

void maximum(vector<double>& maxProperties , const PropertyVector& vec){
    for(unsigned int i = 0 ; i < maxProperties.size() ; i++)
        maxProperties[i] = max(maxProperties[i] , vec.properties[i]);
}

void PropertyVector::print(){ /////For test
    for(auto x = properties.begin() ; x < properties.end() ; x++)
        cout << *x << ',';
    
    cout << this->lastElement << endl;
}

PropertyVector::PropertyVector(const string& csvLine){
    vector<string> extracted = extractCsvLine(csvLine);
    for(unsigned int i = 0 ; i < extracted.size() ; i++){
        if(i == (extracted.size() - 1) )
            this->lastElement = extracted[i];
        else
            this->properties.push_back(stod(extracted[i]) );
    }
}

vector<string> PropertyVector::extractCsvLine(const string& csvLine){
    stringstream stream(csvLine);
    vector<string> extracted;
    string property;
    while(getline(stream , property , ',') )
        extracted.push_back(property);

    return extracted;
}

double PropertyVector::getBias(void){
    return stod(lastElement);
}

int PropertyVector::getPriceRange(void){
    return stoi(lastElement);
}

void extractWeights(const string& weightsVectorFile){
    ifstream csvFile;
    string line;
    csvFile.open(weightsVectorFile , ifstream::in);
    getline(csvFile , line); // dummy line
    while(getline(csvFile , line) )
        Weights.push_back(PropertyVector(line) );
    csvFile.close();
}

void* extractTrainingFile(void* trainVectorFile){
    ifstream csvFile;
    string line;
    csvFile.open((char*) trainVectorFile , ifstream::in );
    getline(csvFile , line); // dummy line
    while(getline(csvFile , line) ){
        PropertyVector newVec = PropertyVector(line);
        minimum(minProperties , newVec);
        maximum(maxProperties , newVec);
        trainData.push_back(newVec);
    }
    csvFile.close();
}

void normalizeAll(void){
    for(unsigned int i = 0 ; i < trainData.size() ; i++)
        trainData[i].normalize(maxProperties , minProperties);
}


void train(const string& trainVectorFile){
    unsigned int size = Weights[0].getSize();
    maxProperties = vector<double>(size , -10e12);
    minProperties = vector<double>(size , 10e12);
    extractTrainingFile((void*) &trainVectorFile[0]);
    normalizeAll();
}

void classify(PropertyVector vec){
    double maxScore = -10e12; int maxRange = 0;
    for(unsigned int i = 0 ; i < Weights.size() ; i++){
        double score = (vec * Weights[i]) + Weights[i].getBias();
        if(maxScore < score ){
            maxScore = score;
            maxRange = i;
        }
    }
    if(maxRange ==  vec.getPriceRange() )
        correctClassifications++;
}

void classifyAll(){
    for(unsigned int i = 0 ; i < trainData.size() ; i++)
        classify(trainData[i]);
}

int main(int argc , char* argv[]){
    double accuracy;
    string folderName = argv[1];
    string weightsVectorFile = folderName + WEIGHT_FILE;
    string trainVectorFile = folderName + TRAIN_FILE;

    extractWeights(weightsVectorFile);
    train(trainVectorFile);
    classifyAll();

    accuracy = ((double) correctClassifications) / trainData.size();

    cout.precision(4);
    cout << "Accuracy: " << accuracy*100 << "%" << endl;
    return 0;
}
